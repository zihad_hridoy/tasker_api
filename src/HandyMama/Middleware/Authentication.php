<?php
/*Add this Middleware for those route that restricted */
namespace  HandyMama\Middleware;

use HandyMama\Controllers\Tasker_api_controller;

class Authentication
{
  public function __invoke($request, $response, $next){


    $tasker_api_controller = new Tasker_api_controller();

    //Parse Authorization header
    $parsed_header = $tasker_api_controller->parse_authorization_header($request);
    $token = $parsed_header['token'];
    $user_id = $parsed_header['user_id'];

    if($tasker_api_controller->authenticate($token, $user_id)){
      //Request authenticated so let it access the protected resource
      $response = $next($request, $response);
      return $response;
    }else{
      //Failed to authenticate the request
      $authentication_result['failed'] = "You are not authorized to perform this action";
      return $response->withStatus(401)->withJson($authentication_result);


    }



  }
}

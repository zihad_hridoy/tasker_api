<?php

namespace HandyMama\Controllers;

use HandyMama\Models\Tasker;
use HandyMama\Models\Lead;
use HandyMama\Models\Job;


class Tasker_api_controller{


  //find_tasker_with_phone ==============================Start|
  public function find_tasker_with_phone($request, $response){

    $data = $request->getParsedBody();

    $phone = $data['phone'];

    //$find_tasker_with_phone_result will hold API responce
    $find_tasker_with_phone_result = array();

    if (Tasker::where('phone1', '=', $phone)->exists()){
      //get the tasker row
      $tasker_row = Tasker::where('phone1', '=', $phone)->first();

      //generate and store verification code in $numeric_code
      $numeric_code = $this->verification_code_generator();

      $verification_code = ['verification_code' => $numeric_code];
      $tasker_row->update($verification_code);

      $find_tasker_with_phone_result['id'] = $tasker_row->tid;
      $find_tasker_with_phone_result['found'] = "Found this tasker";
      $find_tasker_with_phone_result['name'] = $tasker_row->name;
      $find_tasker_with_phone_result['photo'] = $tasker_row->photo;

      //Send the varification code through SMS
      $this->send_sms($phone, $numeric_code);
    }else{
      $find_tasker_with_phone_result['not_found'] = "Sorry you are not registered";
    }

    return $response->withStatus(200)->withJson($find_tasker_with_phone_result);
  }

  //find_tasker_with_phone ==============================End|

  //validate_pin_for_tasker_auth ==============================Start|
  public function validate_pin_for_tasker_auth($request, $response){
    $data = $request->getParsedBody();

    $verification_code = $data['verification_code'];
    $id = $data['id'];

    //$validate_pin_for_tasker_auth_result will hold API responce
    $validate_pin_for_tasker_auth_result = array();
    $verification_data = ['tid' => $id, 'verification_code' => $verification_code];
    if (Tasker::where($verification_data)->exists()){
      // Found that tasker

      //get tasker
      $current_tasker = Tasker::where($verification_data)->first();
      //get tasker id
      $current_tasker_id = $current_tasker[tid];
      $auth_token_arr = array();
      //make the token
      $token = bin2hex(openssl_random_pseudo_bytes(8));

      $token_with_taskerid = $token.'|'.$current_tasker_id;
      //token will expire after 12 week from now
      $token_expiration = date('Y-m-d H:i:s', strtotime('+12 week'));
      //generate $auth_token_arr for updating the currently loggedin tasker
      $auth_token_arr = ['token' => $token_with_taskerid, 'token_expire' => $token_expiration];
      //update $auth_token_arr into authenticated tasker row
      Tasker::where($verification_data)->update($auth_token_arr);
      $validate_pin_for_tasker_auth_result['success'] = "Login success";
      $validate_pin_for_tasker_auth_result['token'] = $token_with_taskerid;

      return $response->withStatus(200)->withJson($validate_pin_for_tasker_auth_result);
    }else{
      //Tasker not found
      $validate_pin_for_tasker_auth_result['fail'] = "Login failed";
      return $response->withStatus(401)->withJson($validate_pin_for_tasker_auth_result);
    }
  }
  //validate_pin_for_tasker_auth ==============================End|

  //accept_job ==============================Start|
  public function accept_job($request, $response){
    $data = $request->getParsedBody();
    //jid of job table
    $id = $data['id'];

    //$accept_job_result will hold API responce
    $accept_job_result = array();
    $accept_job_data = ['status' => 15, 'accepted_by'=>'tasker_app'];

    //update $accept_job_data into Jobs table row
    Job::where('jid', '=', $id)->update($accept_job_data);
    $accept_job_result['success'] = "Job accepted successfully";
    return $response->withStatus(200)->withJson($accept_job_result);

  }
  //accept_job ==============================End|

  //reject_job ==============================Start|
  public function reject_job($request, $response){
    $data = $request->getParsedBody();
    //jid of job table
    $id = $data['id'];

    //$reject_job_result will hold API responce
    $reject_job_result = array();
    $reject_job_data = ['status' => 10];

    //update $reject_job_data into Jobs table row
    Job::where('jid', '=', $id)->update($reject_job_data);
    $reject_job_result['success'] = "Job rejected successfully";
    return $response->withStatus(200)->withJson($reject_job_result);
  }
  //reject_job ==============================End|

  //start_job ==============================Start|
  public function start_job($request, $response){
    $data = $request->getParsedBody();
    //jid of job table
    $id = $data['id'];

    //$start_job_result will hold API responce
    $start_job_result = array();
    //When a job is started it's status will be onprogress and starttime colum of job table will hold current time
    $start_job_data = ['starttime' => date('Y-m-d H:i:s'), 'status' => 12];

    //update $start_job_data into Jobs table row
    Job::where('jid', '=', $id)->update($start_job_data);
    $start_job_result['success'] = "Job started successfully";
    return $response->withStatus(200)->withJson($start_job_result);
  }
  //start_job ==============================End|


  //complete_this_job ==============================Start|
  public function complete_this_job($request, $response){
    $data = $request->getParsedBody();
    //jid of job table
    $id = $data['id'];

    //$complete_this_job_result will hold API responce
    $complete_this_job_result = array();
    //When a job is completed it's status will be completed=13 and endtime colum of job table will hold current time
    $complete_job_data = ['endtime' => date('Y-m-d H:i:s'), 'status' => 13];

    //update $complete_job_data into Jobs table row
    Job::where('jid', '=', $id)->update($complete_job_data);
    $complete_this_job_result['success'] = "Job completed successfully";
    return $response->withStatus(200)->withJson($complete_this_job_result);
  }
  //complete_this_job ==============================End|






/*=================Below are all generic function (e.g. following functions are not route handler)================*/



//parse_authorization_header==============================Start|
/*Purpose of method: Every authenticated request will come with an Authorization header
  This method will parse that header. To follow the DRY principle this methode is defined.
  So any where in the project required to parese Authentication header they will use this method.
*/
public function parse_authorization_header($request){
  $auth = $request->getHeader('Authorization');
  $_auth = $auth[0];

  $parsed_header = array();

  //For this API token is in [ token|id ] format
  //Giving token in this format for the conviniency of client app that will use my API
  $parsed_header['token'] = substr($_auth, strpos($_auth, ' '));
  $parsed_header['user_id'] = substr($_auth, strpos($_auth, '|')+1);


  return $parsed_header;
}

//parse_authorization_header==============================End|

//authenticate==============================Start|
  public function authenticate($token, $user_id){
    $authentication_data = ['tid' => $user_id, 'token' => $token];
    if(Tasker::where($authentication_data)->where('token_expire', '>=', date('Y-m-d H:i:s'))->exists()){
      //**Tasker has authorized so update token expiration time**//
      //token will expire after 12 week from now
      $token_expiration = date('Y-m-d H:i:s', strtotime('+12 week'));//This date format should be similar to previous date format during token creation (signin)

      $token_expiration_data = ['token_expire' => $token_expiration];

      //update token_expire time
      Tasker::where($authentication_data)->update($token_expiration_data);
      return true;
    }

    return false;

  }
//authenticate==============================End|



//Generate Client verification code==============================Start|
/*Generate 4 digit random number*/
    private function verification_code_generator() {
      $verification_code = mt_rand(1000, 9999);
      return $verification_code;
    }
//Generate Client verification code==============================End|

//send_sms==============================Start|
    public function send_sms($user_phone, $verification_code){
      $to =substr($user_phone, -11); //take the last 11 digit

      $custom_message="{$verification_code}";

      $url = "http://107.20.199.106/restapi/sms/1/text/single";
      // $url = str_replace(" ","%20",$sms_api_url);

      $payload = json_encode(array("from" => "HandyMama", "to" => "88$to", "text"=>$custom_message));

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

      //Set your auth headers
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
      	   'Accept: application/json',
          'Authorization: Basic aGFuZHltYW1hOTk6UHdmamlLbjA='
      ));


      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);
      $result = curl_exec($curl);
      curl_close($curl);


      return $result;
    }
    //send_sms==============================End|

    // ===============================Tasker Api Start

    //get_notification===========================Start|
    public function get_notification($request, $response)
    {
      //get authenticate tasker id
      $parsed_header = $this->parse_authorization_header($request);
      $parsed_header_tasker_id = $parsed_header['user_id'];


      $get_data_for_notification = Job::leftJoin('leads', 'jobs.orderid', '=', 'leads.orderid')
                                   ->select('jobs.jid', 'jobs.starttime', 'leads.houseroad', 'leads.leadarea', 'leads.leadcity', 'leads.services')
                                   ->where('jobs.status','11')
                                   ->where('jobs.assignedto', $parsed_header_tasker_id)
                                   ->get();

      $notification_details = array();
      $i = 0;
      foreach ($get_data_for_notification as $row)
      {

        $notification_details['notification_data'][$i]['id'] = $row->jid;
        $notification_details['notification_data'][$i]['services'] = $row->services;
        $notification_details['notification_data'][$i]['address'] = $row->houseroad.','.$row->leadarea.','.$row->leadcity;
        $notification_details['notification_data'][$i]['starttime'] = $row->starttime;

        $i++;
      }
      //Check for if there is no data found
      if (empty($notification_details))
      {
        $notification_details['notification_data'] = "No Data Found";
      }

      //return these data in json and with status
      return $response->withStatus(200)->withJson($notification_details);
    }

    //get_notification ==============================End|


    //work_history===========================Start|

    public function work_history($request, $response)
    {
      //get authenticate tasker id
      $parsed_header = $this->parse_authorization_header($request);
      $parsed_header_tasker_id = $parsed_header['user_id'];

      //get completed jobs and check tasker id with jobs assigned to
      $get_work_history = Job::leftJoin('leads', 'jobs.orderid', '=', 'leads.orderid')
                                   ->select('jobs.jid', 'jobs.starttime', 'jobs.endtime', 'leads.houseroad', 'leads.leadarea', 'leads.leadcity', 'leads.services')
                                   ->where('jobs.status','13')
                                   ->where('jobs.assignedto',$parsed_header_tasker_id)
                                   ->get();

      $history_details = array();
      $i = 0;

      foreach($get_work_history as $row)
      {
        $history_details['completed_jobs'][$i]['id'] = $row->jid;
        $history_details['completed_jobs'][$i]['start_time'] = $row->starttime;
        $history_details['completed_jobs'][$i]['end_time'] = $row->endtime;
        $history_details['completed_jobs'][$i]['services'] = $row->services;
        $history_details['completed_jobs'][$i]['address'] = $row->houseroad. ',' .$row->leadarea. ',' .$row->leadcity;

        $i++;
      }

      //check if no data found
      if (empty($history_details))
      {
        $history_details['completed_jobs'] = "No Data Found";
      }

      //return these data in json and with status
      return $response->withStatus(200)->withJson($history_details);
    }

    //work_history ==============================End|

    //current_work===========================Start|

    public function current_work($request, $response)
    {
      //get authenticate tasker id
      $parsed_header = $this->parse_authorization_header($request);
      $parsed_header_tasker_id = $parsed_header['user_id'];

      //Current work will include those job that has been accepted both started and not started jobs
      //get accepted jobs and check tasker id with jobs assigned to
      $get_current_work = Job::leftJoin('leads', 'jobs.orderid', '=', 'leads.orderid')
                                   ->select('jobs.jid', 'jobs.starttime', 'leads.houseroad', 'leads.leadarea', 'leads.leadcity', 'leads.services')
                                   ->whereIn('jobs.status', [15, 12])
                                   ->where('jobs.assignedto', $parsed_header_tasker_id)
                                   ->get();

      $current_work_details = array();
      $i = 0;

      foreach($get_current_work as $row)
      {
        $current_work_details['current_work'][$i]['id'] = $row->jid;
        $current_work_details['current_work'][$i]['start_time'] = $row->starttime;
        $current_work_details['current_work'][$i]['services'] = $row->services;
        $current_work_details['current_work'][$i]['address'] = $row->houseroad. ',' .$row->leadarea. ',' .$row->leadcity;

        $i++;
      }

      //check if no data found
      if (empty($current_work_details))
      {
        $current_work_details['current_work'] = "No Data Found";
      }

      //return these data in json and with status
      return $response->withStatus(200)->withJson($current_work_details);
    }
    //current_work ==============================End|

    //tasker_profile ============================Start|

    public function tasker_profile($request, $response)
    {
      //get authenticate tasker id
      $parsed_header = $this->parse_authorization_header($request);
      $tasker_id = $parsed_header['user_id'];

      $get_tasker_info = Tasker::where('tid', '=', $tasker_id)->first();

      $tasker_profile = array();

      $tasker_profile['id'] = $tasker_id;
      $tasker_profile['name'] = $get_tasker_info->name;
      $tasker_profile['phone'] = $get_tasker_info->phone1;
      $tasker_profile['address'] = $get_tasker_info->streetno. ',' .$get_tasker_info->area. ',' .$get_tasker_info->city;
      $tasker_profile['work_category'] = $get_tasker_info->wcategory;
      $tasker_profile['photo'] = $get_tasker_info->photo;

      return $response->withStatus(200)->withJson($tasker_profile);

    }

    //tasker_profile ============================End|

}

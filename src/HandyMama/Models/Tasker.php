<?php

namespace HandyMama\Models;

class Tasker extends \Illuminate\Database\Eloquent\Model
{


      protected $primaryKey = 'tid';

      const CREATED_AT = 'created';
      const UPDATED_AT = 'updated';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'verification_code'
      ];

}

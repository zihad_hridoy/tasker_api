<?php

require 'vendor/autoload.php';
include 'bootstrap.php';

use Respect\Validation\Validator as v;
use HandyMama\Middleware\Authentication as hm_auth;




//$config is declared in [config/db_credentials.php]
$app = new \Slim\App(["settings" => $config]);



$container = $app->getContainer();

$container['Tasker_api_controller'] = function(){
  return new \HandyMama\Controllers\Tasker_api_controller;
};

// $container['Test_controller'] = function(){
//   return new \HandyMama\Controllers\Test_controller;
// };

//================================================Rout:Testing Route


// $app->get('/hello/{name}', 'Test_controller:testing_method');
//================================================Rout:End

//================================================Rout:[/auth_phone]

$app->post('/auth_phone', 'Tasker_api_controller:find_tasker_with_phone');
//================================================Rout:End

//================================================Rout:[/auth_phone]

$app->post('/auth_pin', 'Tasker_api_controller:validate_pin_for_tasker_auth');
//================================================Rout:End

//================================================Rout:[/accept_job]
$app->post('/accept_job', 'Tasker_api_controller:accept_job')->add(new hm_auth());
//================================================Rout:End


//================================================Rout:[/reject_job]
$app->post('/reject_job', 'Tasker_api_controller:reject_job')->add(new hm_auth());
//================================================Rout:End


//================================================Rout:[/start_job]
$app->post('/start_job', 'Tasker_api_controller:start_job')->add(new hm_auth());
//================================================Rout:End


//================================================Rout:[/complete_this_job]
$app->post('/complete_this_job', 'Tasker_api_controller:complete_this_job')->add(new hm_auth());
//================================================Rout:End



//================================================Rout:[/get_notification]

$app->get('/get_notification', 'Tasker_api_controller:get_notification')->add(new hm_auth());
//================================================Rout:End


//=================================================Rout:[/work_history]

$app->get('/work_history', 'Tasker_api_controller:work_history')->add(new hm_auth());
//=================================================Rout:End

//=================================================Rout:[/current_work]

$app->get('/current_work', 'Tasker_api_controller:current_work')->add(new hm_auth());
//=================================================Rout:End

//=================================================Rout:[/tasker_profile]

$app->get('/tasker_profile', 'Tasker_api_controller:tasker_profile')->add(new hm_auth());
//=================================================Rout:End


// Run app
$app->run();
